#  Proyecto Punto de Luz #
Proyecto IoT usando ESP8266 e integrado con Node-RED para montar un interfaz de usuario Web (también está el código para integrarlo con Domoticz). El módulo ESP controla un punto de luz mediante una de sus salidas digitales conectada a un rele. 
Además usando su entrada analógica conectada a una foto-resistencia, mide la cantidad de luz en el ambiente. 

En Node-RED tenemos tres flujos. El primero para controlar la luz mediante un interruptor. El segundo recibe las actualizaciones del sensor de luz y muestra estos valores de forma gráfica. Para finalizar un tercer flujo recibe notificaciones del estado del punto de luz (encendido/apagado) y las muestra en pantalla.

En Domoticz tenemos un interuptor que controla el punto de luz de forma remota, lo hace mediante dos scritps bash que se disparan al accionar el interruptor virtual (on/off). El script se encarga de enviar un mensaje MQTT al punto de luz.

Otro  interruptor en domoticz activa el funcionamiento automático del punto de luz en función de la luz ambiente detectada. El control automático de la luz se hace desde Domoticz mediante un script programado en lua.

En el punto de luz además, hay un pulsador local que permite conmutar su estado (on/off). Cuando esto ocurre, el punto de luz  informa a Domoticz del cambio de estado mediante un mensaje MQTT para mantener siempre en el servidor Domoticz el estado actualizado del punto de luz.

* En el directorio ESP están los fuentes lua para el módulo ESP
* En el directorio Node-RED están los flujos que se pueden importar en Node-RED desde el clipboard
* En el directorio domoticz están los scripts para la integración con Domoticz

Usamos MQTT para:

* recibir ordenes remotas
* enviar actualizaciones del estado del interruptor
* enviar periodicamente valores del sensor de luminosidad
