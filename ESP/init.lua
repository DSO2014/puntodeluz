-----------------------------------------------------------------
-- Programa de incio diferido para evitar dejar colgado el micro
-- Informatica Industrial
-----------------------------------------------------------------
MAC= "b8:27:eb:8b:c3:c9"
IP = "172.16.53.27"
TIEMPO = 5000 -- pausa hasta lanzar el programa en segundos
print()
print("*************************************************")
print("** Bienvenid@ al lab de Informatica Industrial **")
print("*************************************************")
print()
print("Activado la WiFi para conectar a un router ...")
wifi.setmode(wifi.STATION)  -- configurando wifi para contectar al router

print("Configurando la MAC address a "..MAC.." ...")
if wifi.sta.setmac(MAC) then print(" OK!, cuando conectes la IP sera : "..IP) -- cambia la MAC
else print("ERROR en la configuracion de MAC") end 

print("Pon en programa.lua tu programa de arranque y no modifiques init.lua")
print("Para detener el arranque utiliza... inicio:stop() ")

inicio=tmr.create() -- crea un temporizador

inicio:register(TIEMPO, tmr.ALARM_SINGLE, 
function (timer) 
        print("Arrancando programa.lua")
        if file.exists("programa.lua") then
            print(" fichero existe!")
            dofile("programa.lua")  -- ejecuta el programa
        else
            print(" fichero no encontrado")
        end
end)

inicio:start()
