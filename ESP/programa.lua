--------------------------------------------------------------
-- Laboratorio de Informatica Industrial
-- Programa ejemplo para control de un punto de luz
-- Se conecta a la WiFi y al broker MQTT del lab
-- Acepta mensajes para apagar/encender la luz
-- Envía periodicamente informacion del nivel de luz
--------------------------------------------------------------

------------------------------
-- Variables con parametros --
------------------------------

SSID = "infind"      -- nombre WiFi
PASSWD = "1518wifi"  -- clave WiFi
MQTT_BROKER = "172.16.53.48" -- direccion de broker MQTT (pi48)
PORT = 1883 -- puerto por defecto para MQTT
TOPIC = "industriales/lab1518/puntodeluz1"  -- topic donde recibir mensajes

RELE = 6    -- Rele conectado a D6
BOTON = 1   -- boton conectado a D1
IDX_luz = 176   -- identificador en Domoticz de nuestro sensor de luz
IDX_int = 177   -- identificador en Domoticz de nuestro interruptor de luz

---------------
-- FUNCIONES --
---------------

-----------------------------------------------
-- Filtra los rebotes en la pusacion de botones
-- copiado de: https://gist.github.com/marcelstoer/59563e791effa4acb65f
function debounce (func)
    local last = 0
    local delay = 500000
    return function (...)
        local now = tmr.now()
        local delta = now - last
        if delta < 0 then delta = delta + 2147483647 end; -- proposed because of delta rolling over, https://github.com/hackhitchin/esp8266-co-uk/issues/2
        if delta < delay then return end;

        last = now
        return func(...)
    end
end

-----------------------------------------------
-- estado = "On" o "Off", enciende o apaga el rele
-- origen = "Local" o "Remoto"
function cambia_rele(status, origin)
    local valor, ok , json
    if(status=="On") then valor=1 else valor=0 end
    -- cambia el estado del rele
    gpio.write(RELE,valor)
    -- si el cambio es de origen local, envia notificación a domoticz
    if(origin=="Local") then
        ok, json = pcall(cjson.encode, {idx=IDX_int, nvalue=valor})
        if ok then
            -- enviamos el mensaje MQTT para informar del estado de la luz
            m:publish("domoticz/in",json,0,0, function(client) print("mqtt sent"..json) end)
        else
            print("Error en codificacion")
        end        
    end
    -- envia notificacion a todos por el topic industriales/lab1518/puntodeluz1/ESTADO
    ok, json = pcall(cjson.encode, {Estado=status,Origen=origin})
    if ok then
         -- enviamos el mensaje MQTT para informar del estado de la luz
        m:publish(TOPIC.."/estado",json,0,0, function(client) print("mqtt sent"..json) end)
    else
        print("Error en codificacion")
    end

end

-----------------------------------------------
-- se ejecuta cada vez que se pulsa el boton (manejador de la interrupcion)
function funcion_pulsador(level)  --conmutar rele
    print("pulsacion")
     -- conmutamos el estado del rele
    if gpio.read(RELE)==0 then
       cambia_rele("On","Local")
    else
       cambia_rele("Off","Local")
    end
end

-------------------------------------------
function procesa_mensaje(client, topic, data) 
    print("mqtt received: "..topic) 
    if(data~=nil) then
        print(" : "..data)
        tabla = cjson.decode(data)
        orden=tabla["Orden"]
        -- conectamos el rele
        if orden=="Enciende" then
            cambia_rele("On","Remoto")
        elseif orden=="Apaga" then
            -- desconectamos el rele
            cambia_rele("Off","Remoto")
        elseif orden=="Conmuta" then
        -- conmutamos el estado del rele
            if gpio.read(RELE)==0 then
                cambia_rele("On","Remoto")
            else
                cambia_rele("Off","Remoto")
           end
        else
            print("Orden no encontrada:")
            print(orden)
        end
    end 
end
-------------------------------------------
function tarea_periodica()
    -- leemos el divisor de tension del sensor de luz
    local sensor = adc.read(0)
    -- codificamos un JSON con los datos para domoticz
    local ok, json = pcall(cjson.encode, {idx=IDX_luz, svalue=string.format("%d",sensor)})
    if ok then
        -- enviamos el mensaje MQTT
        m:publish("domoticz/in",json,0,0, function(client) print("mqtt sent, lux= "..tostring(sensor)) end)
    else
        print("Error en codificacion")
    end        
    ok, json = pcall(cjson.encode, {Sensor="Luz", Valor=sensor})
    if ok then
        -- enviamos el mensaje MQTT
        m:publish(TOPIC.."/sensorluz",json,0,0, function(client) print("mqtt sent, "..json) end)
    else
        print("Error en codificacion")
    end        
end
-------------------------------------------
function exito_conexion(client) 
    print("connected to broker")    
    -- nos suscribimos a todos los topics que empiezan por TOPIC
    m:subscribe(TOPIC.."/orden",1, function(client) print("subscribe success") end)
    -- creamos un temporizador que lanzara la tarea_periodica cada x milisegundos
    temporizador=tmr.create()
    temporizador:register(30000, tmr.ALARM_AUTO, tarea_periodica)
    temporizador:start()                                  
end
-------------------------------------------
function fracaso_conexion(client, reason) 
    print("failed reason: "..reason) 
end
-------------------------------------------

function conectado_a_wifi() 
    print("Status: STATION_GOT_IP")  
    print("SSID  : "..SSID)
    print("IP    : "..wifi.sta.getip()) 
    -- conectamos a mqtt
    m = mqtt.Client("client"..tostring(node.chipid()), 120) -- usamos chipid como identificador unico
    m:on("connect", function(client) print ("connected") end)
    m:on("offline", function(client) print ("offline") end)
    m:on("message", procesa_mensaje ) -- preparamos la funcion que se ejecuta cuando llega un mensaje
    m:connect(MQTT_BROKER, PORT, 0, exito_conexion, fracaso_conexion)
end 

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------

-------------------------
-- EMPIEZA EL PROGRAMA --
-------------------------

--  inicilizando convertidor analogico-digital
if adc.force_init_mode(adc.INIT_ADC) then
    node.restart()  -- si no estaba configurado hay que reiniciar
    return 
end

--  configuramos salida para controlar rele
gpio.mode(RELE, gpio.OUTPUT)

-- configuramos interrupcion para vigilar la pulsacion del boton de conmutacion
gpio.mode(BOTON, gpio.INT, gpio.PULLUP)
-- instalamos el manejador de la interrupcion (por flanco de bajada)
-- cada vez que se pulse la entrada BOTON se lanza la funcion_pulsador
gpio.trig(BOTON,"down",debounce(funcion_pulsador))

-- registro de funciones que se llamaran al cambiar el estado WiFi
-- la importante es la ultima
wifi.sta.eventMonReg(wifi.STA_IDLE, function() print("Status: STATION_IDLE") end)
wifi.sta.eventMonReg(wifi.STA_CONNECTING, function() print("Status: STATION_CONNECTING") end)
wifi.sta.eventMonReg(wifi.STA_WRONGPWD, function() print("Status: STATION_WRONG_PASSWORD") end)
wifi.sta.eventMonReg(wifi.STA_APNOTFOUND, function() print("Status: STATION_NO_AP_FOUND") end)
wifi.sta.eventMonReg(wifi.STA_FAIL, function() print("Status: STATION_CONNECT_FAIL") end)
wifi.sta.eventMonReg(wifi.STA_GOTIP, conectado_a_wifi )

print("Configuracion wifi... SSID: " .. SSID)
wifi.sta.config(SSID,PASSWD) -- configura conexion basica
wifi.sta.eventMonStart(500) -- comienza a monitorizar la WiFi

-- fin del programa
-- pero las funciones que responden a los mensajes y el boton se lanzaran cuando ocurra el evento
-- tambien se ejecuta periodicamente un timer, para informar del nivel de luz

